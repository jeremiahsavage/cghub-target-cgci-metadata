"""
Module for that maps various study ids and descriptions
"""

class Study(object):
    "a description of a study"
    def __init__(self, dbGapAcc, sraAcc, bioProjectAcc, desc):
        self.dbGapAcc, self.sraAcc, self.bioProjectAcc, self.desc = dbGapAcc, sraAcc, bioProjectAcc, desc

    def __str__(self):
        return " ".join(self.dbGapAcc, self.sraAcc, self.bioProjectAcc, self.desc)

class Studies(list):
    "table of studies"
    def __init__(self):
        self.byDbGapAcc = {}
        self.bySraAcc = {}
        self.byBioProjectAcc = {}

    def add(self, dbGapAcc, sraAcc, bioProjectAcc, desc):
        study = Study(dbGapAcc, sraAcc, bioProjectAcc, desc)
        self.append(study)
        self.byDbGapAcc[study.dbGapAcc] = study
        if study.sraAcc != None:
            self.bySraAcc[study.sraAcc] = study
        if study.bioProjectAcc != None:
            self.byBioProjectAcc[study.bioProjectAcc] = study

    def getByAnyAcc(self, acc):
        "look up by any type of acc"
        study = self.byDbGapAcc.get(acc)
        if study == None:
            study = self.bySraAcc.get(acc)
        if study == None:
            study = self.byBioProjectAcc.get(acc)
        return study

studies = Studies()
# TARGET
studies.add("phs000218", None, "PRJNA74789", "TARGET")
#studies.add("phs000218", None, None, "TARGET")
studies.add("phs000515", "SRP036598", "PRJNA172041", "TARGET (AML II)")
studies.add("phs000463", "SRP011998", "PRJNA89519", "TARGET (ALL I)")
studies.add("phs000464", "SRP011999", "PRJNA89529", "TARGET (ALL II)")
studies.add("phs000465", "SRP012000", "PRJNA89525", "TARGET (AML)")
studies.add("phs000466", "SRP012001", "PRJNA89537", "TARGET (CCSK)")
studies.add("phs000467", "SRP012002", "PRJNA89523", "TARGET (NBL)")
studies.add("phs000468", "SRP012003", "PRJNA89527", "TARGET (OS)")
studies.add("phs000469", "SRP012004", "PRJNA89535", "TARGET (PPTP)")
studies.add("phs000470", "SRP012005", "PRJNA89539", "TARGET (RT)")
studies.add("phs000471", "SRP012006", "PRJNA89521", "TARGET (WT)")

# CGCI
studies.add("phs000235", "SRP001599", "PRJNA74797", "CGCI")
studies.add("phs000532", "SRP020237", "PRJNA172563", "CGCI (NHL-DLBCL)")
studies.add("phs000533", "SRP020238", "PRJNA172564", "CGCI (NHL-FL)")
studies.add("phs000529", "SRP018841", "PRJNA172565", "CGCI (HTMCP-DLBCL)")
studies.add("phs000530", "SRP036561", "PRJNA172566", "CGCI (HTMCP-LC)")
studies.add("phs000531", "", "PRJNA172567", "CGCI (Medulloblastoma)")
studies.add("phs000534", "", "PRJNA172568", "CGCI (Early Functional Genetic...in HLC)")
studies.add("phs000528", "", "PRJNA172569", "CGCI (HTMCP-CC)")
studies.add("phs000527", "", "PRJNA244444", "CGCI (Burkitt Lymphoma Genome...)")

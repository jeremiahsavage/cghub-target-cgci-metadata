#!/bin/bash

date=`date +%Y_%m_%d`
mkdir /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/summary/latest.$date
cd /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/summary/latest.$date

#remember to link in the latest set of files on the FS that were downloaded from SRA
#REMEMBER to keep this file up to date after every SRA download!!!
ln -s ../pending-download_mark_cwilks.files
#cgquery -Aa -o target.sra.xml 'study=(phs0004* OR phs0005* OR TARGET_* OR CGCI_* OR phs000218 OR phs000235)'
ln -s ../latest.2015_06_18.old/target.sra.xml
ln -s ../latest.2015_06_18.old/SRA_Accessions.tab
ln -s ../latest.new_method/SRA_Accessions.target-cgci.tab
#wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/reports/Metadata/SRA_Accessions.tab
/usr/local/bin/python /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/cghubMetadataCollect.py ./target.sra.xml ./cghub-state.tsv
#/usr/local/bin/python /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/sraAccessionsFilter.py ./SRA_Accessions.tab ./SRA_Accessions.target-cgci.tab
#DEPRECATED for new (analyzeReport.py) method, left for backwards compatibility
/cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/tawk 'NR==1||FNR>1' /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/metadata/*/metadata.status.TARGET.txt  >./all.metadata.status.txt

#get latest released from manifest file (could be up to 12 hours old)
#DEPRECATED for new (analyzeReport.py) method, left for backwards compatibility
egrep -e '^TARGET	' /cghub/home/cwilks/www/html/ws/reports/SUMMARY_STATS/LATEST_MANIFEST.tsv | egrep -e '	Live	' | cut -f 14 > released.LATEST_MANIFEST.tsv.all_live_TARGET.full.files

#produce status list of pending files
#DEPRECATED for new (analyzeReport.py) method, left for backwards compatibility
/cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/produce_target_cgci_accounting_for_mark.pl ./pending-download_mark_cwilks.files /cghub/home/cwilks/target/tracking/validation/new_bam_errors.formatted ./released.LATEST_MANIFEST.tsv.all_live_TARGET.full.files /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/summary/all_bams_under_markd_master.sorted.split.tsv_reported_to_mark_via_gdocs.cgci > pending-download-import-status.tsv

#get library strategies, studies, barcodes, and tumor/tss codes
/cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/determine_new_srxs.sh ./SRA_Accessions.target-cgci.tab ../sra_metadata_all

#now run the integration step
#DEPRECATED for new (analyzeReport.py) method, left for backwards compatibility
#/usr/local/bin/python /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/psychoanalyzeTarget.py ./pending-download_mark_cwilks.files pending-download-import-status.tsv cghub-state.tsv all.metadata.status.txt SRA_Accessions.target-cgci.tab ../sraSkipList target-report.tsv

#new method
/usr/local/bin/python /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/analyzeTarget.py ./pending-download_mark_cwilks.files ./cghub-state.tsv ./SRA_Accessions.target-cgci.tab ../sraSkipList > target-report.tsv 2> target-report.tsv.suppressed_and_misc

#generate various report TSVs
/usr/local/bin/python /cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/scripts/psychoanalyzeReport.py ./target-report.tsv report ../sra_metadata_all/all_target_cgci_srxs.lib_strats ../sra_metadata_all/all_target_cgci_srxs.barcodes_studies_tss_codes

#now get a list of all the data that is absent from cghub's disks
cut -f 2 report/center-state.report.tsv | sort -u | perl -ne 'chomp; `cut -f 1 report/$_-state.details/*-absent.tsv >> all_absent_srrs.tsv_`;'

sort -u all_absent_srrs.tsv_ > cghub_absent_srrs.$date.tsv
#all_centers=`ls report/*-state-disease-seqtype.report.tsv | perl -ne 'chomp; print "-a $_ ";'`
tar -cvf ./center_specific_detail_reports.tar report/center-state.report.tsv report/*-state-disease-seqtype.report.tsv
gzip ./center_specific_detail_reports.tar
#email the resulting file to forward to SRA
#echo "latest TARGET and CGCI stats $date" | /usr/bin/mutt -s "Weekly New TARGET/CGCI SRR at SRA Check for $date" -a cghub_absent_srrs.$date.tsv -a report/center-state.report.tsv $all_centers -- cwilks@soe.ucsc.edu ekephart@ucsc.edu
echo "latest TARGET and CGCI stats $date" | /usr/bin/mutt -s "Weekly New TARGET/CGCI SRR at SRA Check for $date" -a cghub_absent_srrs.$date.tsv -a ./center_specific_detail_reports.tar.gz -- cwilks@soe.ucsc.edu ekephart@ucsc.edu

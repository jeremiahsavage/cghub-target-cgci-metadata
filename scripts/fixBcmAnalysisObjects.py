#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)


 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("perObjectDir", type=str, 
                        default="metadata/fromCenters/BCM/perObject",
                        help="Directory with each type of metadata object") 
    parser.add_argument("refcenter", type=str, default="NCI", 
                        help="New value to specify under STUDY_REF")
    args = parser.parse_args()

    for analysisFile in os.listdir(args.perObjectDir + "/analysis"):
        analysisPath = args.perObjectDir + "/analysis/" + analysisFile
        analysisTree = readXmlFile(analysisPath)
        analysisXml = analysisTree.getroot()
        studyRef = analysisXml.find("STUDY_REF")
        studyRef.set("refcenter", args.refcenter)
        for runRef in analysisXml.iter("RUN"):
            runAccession = runRef.get("accession")
            runPath = args.perObjectDir + "/run/" + runAccession + ".xml"
            runXml = readXmlFile(runPath).getroot()
            expRef = runXml.find("EXPERIMENT_REF")
            refname = expRef.get("refname")
            runRef.set("read_group_label", refname)
        analysisTree.write(analysisPath)
        
if __name__ == '__main__':
    main()

    


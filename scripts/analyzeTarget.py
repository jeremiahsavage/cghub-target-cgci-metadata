#!/usr/bin/python


#redone version of Marks psychoanalyzeTarget.py

#THE PRIMARY GOAL of this script is to produce a reliable list of (in priority order, though 1 and 2 are related):
#1) SRRs (files) at SRA which need to be downloaded
#2) downloads from SRA that need to be released
#3) downloads from SRA which have already been released (and in some cases, the source files removed from the orignial download locations)

#data model:

#Mark's state values: sraStatus:(live,unknown), metadataState:(ok,error), downloadedState:(ok,error), cghubState:(ok,error)
#Mark's state description values: sraInfo:(multiLiveExpr,''), metadataDesc:(OK,live,Failed,noMetadata), downloadedDesc:(absent,dupsFlaggedBam,fastqSubmission,partialChromSplitBam,chromSplitBam,released,singleBam,srfSubmission), cghubDesc:(live,notLoaded,suppressed,uploading)
#2 Metadata states: "ready" (metadata good) "not_ready" (metadata missing or errored)
#2 SRA accessions (we care about): SRR (run accession, this is the atomic id for SRA) SRX (experiment accession this can be used to group related runs together, useful for grouping muliple SRFs)
#3 Files Types: BAMs (.bam), FASTQs (.tar, .gz), SRFs (.srf)
#DEPRECATED 4 basic OUTPUT data states: "live" (released to CGHub) "ready" (on CGHub's disks, but not released yet) "absent" (not on CGHubs disks nor released) "suppressed" (present at CGHub either released or just on disk, but suppressed at SRA)

#1 SRX = >=1 SRR
#1 SRR = 1 BAM or SRF, 1-2 FASTQs (since sometimes the read pairs are broken into read1 and read2 files)

#Special case center: CGI:
#due to transfer issues, CGI broke up their original submissions into per-chromsome BAM files (including one for notMapped reads)#this means that multiple BAMs will map to one SRR accession in the case of CGI only.  To further complicate this, CGI was known to submit a second version of a submission with a new SRR accession but with the same filenames either to *replace* the original or *supplement* the original, its not entirely clear which ones are which.  This is a data quality issue going back to SRA/CGI and can not be worked out by CGHub.  However in practice there were clear issues with certain of these duplicate sets which meant we removed one set from the release and put it into TARGET_deprecated study.

#This script assumes the following:

#1) SRFs which share an SRX should be grouped into one submission (even if they have different SRRs).
#this is because early on SRFs were apparently generated per lane so they act as one file per read group, which should be consolidated into one just like BAMs
#2) BAMs and FASTQs which share an SRX should be kept as separate submissions
#3) SRRs which are suppressed at SRA should be used to flag any live, or ready data at CGHub to be suppressed, but otherwise ignored for everything else
#4) SRXs which are suppressed at SRA should mean that all associated SRRs are suppressed, if this is not true this is flagged as an anomaly
#5) barcode and study are derived directly from the SRA metadata, not from the filepath
#6) all TARGET/CGCI files at CGHub should have an associated SRX and SRR accession, for those which do not these are considered exceptional cases and need to be dealt with by hand
#7) CGI files are grouped by SRR, in the case where there are multiple matching CGI filenames with different paths and the breakdown of SRR accession is not clear, this will be flagged for manual resolution
#8) a skip list of SRRs is used to remove from *any* consideration those accessions deemed irrelevant (e.g. exact duplicates of ones we already have)

import sys
import os
import re
from collections import namedtuple
import csv
from collections import OrderedDict
from OrderedSet import OrderedSet
import argparse
from studies import studies

#live_srrs and non_live_srrs are dicts which map to the SRX for that SRR, live_srxs and non_live_srxs are sets
SRAAccessionStatus = namedtuple('SRAAccessionStatus', 'live_srrs live_srxs non_live_srrs non_live_srxs live_at_cghub')
SRARunRow = namedtuple('SRARunRow', 'runAcc exprAcc center bioproject replacedby')
#all three fields are sets
CGHubStatus = namedtuple('CGHubStatus', 'live_analyses non_live_analyses to_suppress')
#helper tuple to track state and analysis uuid
CGHubState = namedtuple('CGHubState', 'state analysisId')

#to stay in conformance with the format that Mark's (and downstream formatters) psychoanalyzeReport scripts expect
ReportPrimitive = namedtuple('ReportPrimitive', 'runAcc exprAcc sampleAcc runSubmissionAcc sraStatus sraInfo study center sampleBarcode metadataState metadataDesc metadataGitId downloadedState downloadedDesc cghubState cghubDesc analysisId allRunAccs allExprAccs downloadedDirs filenames filetype')
reportHeader = ReportPrimitive._fields
#setup the base default RP, values which parrot the field name will be replaced with actual values, those with '' will in general NOT be filled with any value:
default_rp = ReportPrimitive(runAcc='runAcc', exprAcc='exprAcc', sampleAcc='', runSubmissionAcc='', sraStatus='unknown', sraInfo='', study='study', center='center', sampleBarcode='sampleBarcode', metadataState='ok', metadataDesc='noMetadata', metadataGitId='', downloadedState='ok', downloadedDesc='absent', cghubState='ok', cghubDesc='notLoaded', analysisId='', allRunAccs=OrderedSet(), allExprAccs=OrderedSet(), downloadedDirs=OrderedSet(), filenames=OrderedSet(), filetype='')

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""Take various files that indicate what is know about TARGET data and metadata and correlate into a report.""")
    parser.add_argument("downloadedFiles", type=str,
                        help="""file containing paths of BAMs downloaded from NCBI. Generally, these will be ones that have not been submitted to CGHub, however it will report on ones that have been submitted but not purged.  This should include the directory path from NCBI, as it is searched for accessions.  Normally ./inputs/pending-download.files.  A line starting with `#' is considered a comment.""")
    parser.add_argument("cghubStatusTsv", type=str,
                        help="""cghub status information create by cghubMetadataCollect.py""")
    parser.add_argument("sraAccessionsTsv", type=str,
                        help="""SRA_Accessions.tab, MUST be process by sraAccessionsFilter.py, see documentation in the program for why""")
    parser.add_argument("sraAccessionsSkipListTsv", type=str,
                        help="""list of either SRR  or SRX accessions to ignore.  usually this is because they have exact (matching MD5) duplicates""")
    return parser.parse_args()

def load_skip_list(skipListTSV):
    with open(skipListTSV,"r") as f:
        skipList = {x.rstrip(): 1 for x in f.readlines()}
    return skipList

#from Mark
def studyToDesc(acc):
    "look up by all types of studies, default to returning acc"
    study = studies.getByAnyAcc(acc)
    if study == None:
        return acc
    else:
        return study.desc
#we track SRA accessions (SRR, SRX) from the directory path elements
def parse_accessions_from_path(path_elems):
    srx = None
    srr = None
    for pelem in path_elems:
        if pelem.startswith("SRX"):
            srx = pelem
        elif pelem.startswith("SRR"):
            srr = pelem
    return (srx, srr)

multi_value_columns = set(["downloadedDirs", "filenames", "allRunAccs", "allExprAccs"])
def write_report_line(fields, outstream=sys.stdout):
    first = True
    for column in reportHeader:
        field = ""
        if column in fields:
            field = fields[column]
            if column in multi_value_columns:
                field = ",".join(field)
        if not first:
            outstream.write("\t")
        first = False
        if column == 'study':
            field = studyToDesc(field)
        outstream.write(field) 
    outstream.write("\n")

#update the various (4) status fields (description) based on where this accession ended up
def check_and_update_status_report_fields(rp, accession, accession_state_map, map_type):
    if accession not in accession_state_map:
        return
    rp = rp._replace(sraStatus='live', downloadedDesc='released')
    if map_type == 'live_at_cghub':
        #need to update that its at cghub but not in download lists
        rp = rp._replace(metadataDesc='live', cghubDesc='live', analysisId=accession_state_map[accession])
    elif map_type == 'non_live_analyses':
        #set cghubState to "error" because these should be live or there is an error somewhere (assumes the report will not be generated in the middle of a release), also these could include ones with a metadata error (ence the Failed)
        rp = rp._replace(metadataDesc='Failed', cghubState='error', cghubDesc=accession_state_map[accession].state, analysisId=accession_state_map[accession].analysisId)
    return rp

#track multi fastq files to one srr as well as single files
def multi_file_grouper(report_fields, accession, accession_map, accession_type, sra_status, cg_status):
    #first update the state fields for the report
    if accession in sra_status.live_at_cghub:
        report_fields = check_and_update_status_report_fields(report_fields, accession, sra_status.live_at_cghub, 'live_at_cghub')
    elif accession in cg_status.non_live_analyses:
        report_fields = check_and_update_status_report_fields(report_fields, accession, cg_status.non_live_analyses, 'non_live_analyses')
    
    #find out if we already saw this accession (SRR or SRX), if so merge
    if accession in accession_map:
        original_report_fields = accession_map[accession]
        #make sure we're correct matched or else flag and skip
        if report_fields.exprAcc != original_report_fields.exprAcc:
            sys.stderr.write("MISMATCHED_EXPERIMENT_IN_MULTI_FILE_PROCESSOR\t%s\t%s\t%s\n" % (report_fields.runAcc, report_fields.exprAcc, original_report_fields.exprAcc))
            return
        if report_fields.center != original_report_fields.center:
            sys.stderr.write("MISMATCHED_CENTER_IN_MULTI_FILE_PROCESSOR\t%s\t%s\t%s\n" % (report_fields.runAcc, report_fields.center, original_report_fields.center))
            return
        if report_fields.study != original_report_fields.study:
            sys.stderr.write("MISMATCHED_BIOPROJECT_IN_MULTI_FILE_PROCESSOR\t%s\t%s\t%s\n" % (report_fields.runAcc, report_fields.study, original_report_fields.study))
            return
        
        original_report_fields.filenames.update(report_fields.filenames)
        original_report_fields.downloadedDirs.update(report_fields.downloadedDirs)
        if accession_type == 'SRX':
            original_report_fields.allRunAccs.update(report_fields.allRunAccs)
    else:
        #otherwise we have a new (single) report
        accession_map[accession] = report_fields


srx_map = {}
def file_grouper_by_srx(report_fields, sra_status, cg_status):
    multi_file_grouper(report_fields, report_fields.exprAcc, srx_map, 'SRX', sra_status, cg_status)
    
srr_map = {}
def file_grouper_by_srr(report_fields, sra_status, cg_status):
    multi_file_grouper(report_fields, report_fields.runAcc, srr_map, 'SRR', sra_status, cg_status)

#SRFs use the multi file-by-SRX tracker which will group by SRX rather than SRR
groupfunction_by_filetype = {'SRF':file_grouper_by_srx}
def group_by_filetype(report_fields, sra_status, cg_status):
    try:
        group_func = groupfunction_by_filetype[report_fields.filetype]
    #we default to the srr file processor for unknown centers, this tracks where there are multiple different files which share the same SRR accession (currently only FASTQs and CGI BAMs)
    except KeyError, e:
        group_func = file_grouper_by_srr
    group_func(report_fields, sra_status, cg_status) 


file_type_map={"bam":"BAM", "srf":"SRF", "fastq":"FASTQ", "gz":"FASTQ", "tar":"FASTQ"}
#CGI filename pattern example = GS01861-DNA_C03
cgi_filename_pattern=re.compile(r'^GS\d+-DNA_\w\w\w')
def parse_file_type_from_path_and_center(filename, center):
    ftype = ""
    felems = filename.split(".")
    fext = felems[-1]
    if cgi_filename_pattern.search(filename) and center == 'CompleteGenomics':
        return "CGI_BAM"
    try:
        ftype = file_type_map[fext.lower()]
    except KeyError:
        ftype=""
    return ftype

file_type2downloaded_status_map={"":"", "CGI_BAM":"chromSplitBam", "BAM":"singleBam", "SRF":"srfSubmission", "FASTQ":"fastqSubmission"}
def process_downloaded_file(sra_status, cg_status, line):
    path_elems = line.split('/')
    filename = path_elems[-1]
    num_elems = len(path_elems)-1
    dirpath = '/'.join(path_elems[0:num_elems])
    (srx, srr) = parse_accessions_from_path(path_elems)
    #flag but skip those missing one of the critical accessions
    if not srx or not srr:
        sys.stderr.write("MISSING_ACCESSION\t%s\t%s\t%s\n" % (line,srx,srr))
        return
    #flag but skip if not live at SRA, but only if not already caught in the cghub release load
    if srr not in cg_status.to_suppress and (srr in sra_status.non_live_srrs or srx in sra_status.non_live_srxs):
        sys.stderr.write("NOT_LIVE_AT_SRA\t%s\t%s\t%s\n" % (line,srx,srr))
        return
    #from now on we ignore suppressed at SRA
    #but if we can't find our SRR, need to flag and skip it
    if srr not in sra_status.live_srrs:
        sys.stderr.write("NOT_AT_SRA\t%s\t%s\t%s\n" % (line,srx,srr))
        return
    sra_runrow = sra_status.live_srrs[srr]
    if sra_runrow.exprAcc != srx:
        sys.stderr.write("MISMATCHED_SRX\t%s\t%s\t%s\t%s\n" % (line,srx,srr,sra_runrow.exprAcc))
        return
    #create the initial seed sets, these *may* be added to as we find other files sharing the same accession
    #we use ordered sets to track the individual items as a group, could've been done differently of course
    filenames = OrderedSet([filename])
    dirpaths = OrderedSet([dirpath]) 
    allRunAccs = OrderedSet([srr])
    allExprAccs = OrderedSet([srx])
    filetype = parse_file_type_from_path_and_center(filename,sra_runrow.center)
    download_status = file_type2downloaded_status_map[filetype]
    #update the default ReportPrimitive to customize...
    rp = default_rp._replace(downloadedDirs=dirpaths, filenames=filenames, exprAcc=srx, runAcc=srr, allRunAccs=allRunAccs, allExprAccs=allExprAccs, filetype=filetype, study=sra_runrow.bioproject, center=sra_runrow.center, sraStatus='live', downloadedDesc=download_status)
    #determine which tracker to use and then run it
    group_by_filetype(rp, sra_status, cg_status)      
         

#walk through and add from the list of files downloaded from SRA *by their accessions*
def process_downloaded(sra_status, cg_status, downloaded_filesF):
   with open(downloaded_filesF,"r") as f:
       for line in f:
           line = line.rstrip()
           process_downloaded_file(sra_status, cg_status, line)
       #once all processing is done, print out ones remaining in the multi entry maps
       for srr in srr_map.keys():
           write_report_line(srr_map[srr]._asdict())
       #these should ONLY be SRFs at this point
       for srx in srx_map.keys():
           #first we add all the SRR accessions we have to srr_map, which doubles (now) as a tracker for whats already been output (MUST do this after we've already output all the srrs in srr_map)
           for srr_ in srx_map[srx].allRunAccs:
               srr_map[srr_]=srx_map[srx]
           write_report_line(srx_map[srx]._asdict())
       #now find the ones CGHub *DOESNT* have:
       for (srr,runrow) in sra_status.live_srrs.iteritems():
           #check the groups to see if we have it either just downloaded or released
           #have to check both downloaded and released because somethings are downloaded but not released AND some things are released but have been removed from the list of downloads
           if runrow.runAcc in srr_map.keys():
               continue
           #setup the new report fields object
           rp = default_rp._replace(sraStatus='live', exprAcc=runrow.exprAcc, runAcc=runrow.runAcc, study=runrow.bioproject, center=runrow.center)
           #we need to set certain status and id fields if this SRR is either 1) live at cghub or 2) tracked as not live at CGHub
           if runrow.runAcc in sra_status.live_at_cghub:
               rp = check_and_update_status_report_fields(rp, runrow.runAcc, sra_status.live_at_cghub, 'live_at_cghub')
           elif runrow.runAcc in cg_status.non_live_analyses:
               rp = check_and_update_status_report_fields(rp, runrow.runAcc, cg_status.non_live_analyses, 'non_live_analyses')
           write_report_line(rp._asdict())       
    
#from Mark
def fixEmptySRAFields(fieldnames, tsvRow):
    "set columns with are empty or `-' to None"
    for col in fieldnames:
        cell = tsvRow[col]
        if cell in ("", "-"):
            tsvRow[col] = None

#collect the various SRR and SRXs in the SRA tab file            
def process_sra_row(tsvRow, status):
    state = tsvRow["Status"]
    type = tsvRow["Type"]
    if type=="RUN":
        runrow = SRARunRow(runAcc=tsvRow['Accession'], exprAcc=tsvRow['Experiment'], center=tsvRow['Center'], bioproject=tsvRow['BioProject'], replacedby=tsvRow['ReplacedBy'])
        if state=='live':
            status.live_srrs[tsvRow['Accession']]=runrow
        else:
            status.non_live_srrs[tsvRow['Accession']]=runrow
    #only track SRXs for the ones which are non live (if an SRX is not live, we want to suppress all associated SRRs), otherwise the SRX tracking shouldn't be used for anything
    elif tsvRow["Type"] == "EXPERIMENT":
        if state=='live':
            status.live_srxs.add(tsvRow['Accession'])
        else:
            status.non_live_srxs.add(tsvRow['Accession'])

#from Mark 
class excel_tab_noquoting(csv.excel_tab):
    """SRA_Accessions has columns like `"Institute of Vegetables and Flowers,
    Chinese Acad', so this just disables quote processing.  """
    quoting = csv.QUOTE_NONE

def load_sra_accessions(sraF, skipList):
    live_srrs = OrderedDict()
    non_live_srrs = OrderedDict()
    live_srxs = OrderedSet()
    non_live_srxs = OrderedSet()
    live_at_cghub = {}
    
    status = SRAAccessionStatus(live_srrs=live_srrs, non_live_srrs=non_live_srrs, live_srxs=live_srxs, non_live_srxs=non_live_srxs, live_at_cghub=live_at_cghub)
    #from Mark:
    # n.b. file from NCBI has ms-dos newlines
    # n.b. do *not* filter by study, as some suppressed don't have a study
    #      sraAccessionsFilter handles this.
    with open(sraF, "U") as tsvFh:
        tsv = csv.DictReader(tsvFh, dialect=excel_tab_noquoting)
        for tsvRow in tsv:
            fixEmptySRAFields(tsv.fieldnames, tsvRow)
            if tsvRow['Accession'] not in skipList:
                process_sra_row(tsvRow,status)
    return status

#collect all the files from SRA that we have released to GNOS
#the key here is that their metadata really needs to reference the SRR(s) and SRX(s) in run and experiment (respectively) for this to work the best
def process_cghub_row(sra_status, cg_status, tsvRow):
    srrs = tsvRow['run_accessions'].split(',')
    srxs = tsvRow['expr_accessions'].split(',')
    cst = tsvRow['state']
    aid = tsvRow['analysis_id']
    study = tsvRow['study']
    clive = False
    not_found = True
    for srr in srrs:
        #status = CGHubStatus(live_analyses, non_live_analyses, to_suppress)
        if srr in sra_status.live_srrs:
            if cst != 'live' or '_deprecated' in study:
                cg_status.non_live_analyses[srr]=CGHubState(aid, cst)
            else:
                cg_status.live_analyses[srr]=aid
                clive = True
            not_found = False
    for srr in srrs:
        if clive:
            sra_status.live_at_cghub[srr]=aid
       #suppress anything thats not live
        elif not_found and cst != 'suppressed':
            cg_status.to_suppress[srr]=aid
     
def load_cghub_released_analyses(sra_status, cghubStatusTsv):
    live_analyses = {}
    non_live_analyses = {}
    to_suppress = {}
    
    status = CGHubStatus(live_analyses, non_live_analyses, to_suppress)
    with open(cghubStatusTsv, "U") as tsvFh:
        tsv = csv.DictReader(tsvFh, dialect=excel_tab_noquoting)
        for tsvRow in tsv:
             process_cghub_row(sra_status, status, tsvRow)
    return status

def main():
    args = cmdParse()
    sys.stdout.write("%s\n" % ("\t".join(reportHeader)))

    sraSkipList = load_skip_list(args.sraAccessionsSkipListTsv)
    sra_status = load_sra_accessions(args.sraAccessionsTsv, sraSkipList)
    cg_status = load_cghub_released_analyses(sra_status, args.cghubStatusTsv)
    process_downloaded(sra_status, cg_status, args.downloadedFiles)

    #output "" for: sampleAcc, runSubmissionAcc, metadataGitId, study, sampleBarcode.  we'll pick up the last two in the report generation (next script after this one)

if __name__=='__main__':
    main();

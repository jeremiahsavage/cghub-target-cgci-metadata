#!/usr/bin/perl
#used to rationalize the paths to all files downloaded from NCBI SRA for the purpose of migrating into CGHub (mainly TARGET and CGCI studies).  This is necessary because Mark's psychoanalyze reporting scripts rely on the paths to group files
use strict;

my $IMMUT_CMD='/usr/lpp/mmfs/bin/mmchattr -i';
#my $ROOT_DOWNLOADS_PATH="/cghub/home/cwilks/git_cghub/cghub-target-cgci-metadata/summary/all_sra_downloads";
my $ROOT_DOWNLOADS_PATH="/cghub/sra_downloads";

my $complete_file_listF = shift;

main();

sub main
{
	process_complete_downloaded_sra_file_list($complete_file_listF);
}

sub process_complete_downloaded_sra_file_list
{
	my $file = shift;
	open(IN,"<$file");
	while(my $line = <IN>)
	{
		chomp($line);
		my @f = split(/\//,$line);
		#my $srx_index = -1;
		#my $srr_index = -1;
		my $srr;
		my $srx;
		for(my $i = 0; $i < scalar (@f); $i++)
		{
			my $pelem = $f[$i];
			#if($pelem =~ /^TARGET/ || $pelem =~ /^SRP/ || $pelem =~ /^SRS/ || $pelem =~ /^SRX/ || $pelem =~ /^SRR/)
			if($pelem =~ /^SRX/)
			{
				#$srx_index = $i;
				$srx = $pelem;
			}
			if($pelem =~ /^SRR/)
			{
				#$srr_index = $i;
				$srr = $pelem;
			}
			#last if($srr_index > -1 && $srx_index > -1);
			last if($srr && $srx);
		}
		my $file_ = pop(@f);
		#$file_ .= ".md5";
		my $srr_ = pop(@f);
		my $path_ = join("/",@f);
		my $path = "/cghub/home/cwilks/target/$path_/$file_";
		print "$ROOT_DOWNLOADS_PATH/$srx\n";
		`mkdir $ROOT_DOWNLOADS_PATH/$srx` if(! -e "$ROOT_DOWNLOADS_PATH/$srx");
		`mkdir $ROOT_DOWNLOADS_PATH/$srx/$srr` if(! -e "$ROOT_DOWNLOADS_PATH/$srx/$srr");
		if(-e "$ROOT_DOWNLOADS_PATH/$srx/$srr/$file_")
		{
			print STDERR "DUPLICATE_FILENAME\t$line\n";
			next;
		}
		my @r=`$IMMUT_CMD 'no' $path`;
		#print "setting immutability $path $state output: ";
		#print @r;
		#print "\n";
		`ln $path $ROOT_DOWNLOADS_PATH/$srx/$srr/`;
		@r=`$IMMUT_CMD 'yes' $path`;
	}
	close(IN);
}
		

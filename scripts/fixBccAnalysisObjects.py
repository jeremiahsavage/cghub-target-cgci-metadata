#!/usr/bin/env python

import argparse
import common
import copy
import csv
from datetime import datetime
import os
import re
import xml.etree.ElementTree as ET
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def simplifyKeys(oldDictOfDicts):
    """Given a dict of dicts of the manifest, indexed by pathnames, return
    an improved dict of dicts indexed by filenames"""
    newDictOfDicts = dict()
    for pathname in oldDictOfDicts.keys():
        filename = pathname.split("/")[-1]
        newDictOfDicts[filename] = oldDictOfDicts[pathname]
    return newDictOfDicts


def getManifestEntry(analysis, manifestByFilename):
    """Get the entry for this object from the manifest"""
    fileEntry = analysis.find("DATA_BLOCK/FILES/FILE")
    if fileEntry is not None:
        filename = fileEntry.get("filename")
        if manifestByFilename.has_key(filename):
            manifestThisFile = manifestByFilename[filename]
            return(manifestThisFile)
    return(None)
    


def getRunObject(analysis, manifestThisFile, inputDir):
    """Return the run object that matches the indicated analysis object"""
    run = None
    runAccession = manifestThisFile["sra_accession"]
    runXmlPathname = inputDir + "/run/" + runAccession + ".xml"
    if not os.path.exists(runXmlPathname):
        print "Error: cannot find run obj", runXmlPathname
    else:
        run = ET.parse(runXmlPathname).getroot()
    return run


def getBamFile(analysis, manifestByFilename):
    """Return the BAM file from the indicated analysis object"""
    fileEntry = analysis.find("DATA_BLOCK/FILES/FILE")
    if fileEntry is not None:
        filename = fileEntry.get("filename")
        return(filename)
    else:
        return(None)

def addRunLabelsIfNeeded(refAlign, run):
    """If this object contains no run reference, add one"""
    if refAlign.find("RUN_LABELS") is None:
        refname = run.get("alias")
        readGroupLabel = re.sub(".bam$", "", refname)
        runReferenceAttributes = {"accession": run.get("accession"),
                                  "refcenter": run.get("center_name"),
                                  "refname": refname,
                                  "read_group_label": readGroupLabel}
        runLabels = ET.Element("RUN_LABELS")
        runReference = ET.SubElement(runLabels, "RUN", 
                                     attrib = runReferenceAttributes)
        refAlign.insert(1, runLabels)



def addSeqLabelsIfNeeded(refAlign, inputDir):
    """If this object has no SEQ_LABELS section, add one for the appropriate
    reference alignment, using data from the center"""
    if refAlign.find("SEQ_LABELS") == None:
        addSeqLabels(refAlign, inputDir)



def addTargetRefnameIfNeeded(analysis, manifestEntry):
    """If the TARGET object has no refname, then add one from the manifest
    data.  Do not use the 'submitted_sample_id', as this barcode should refer
    to a DNA or RNA analyte but teh submitted_sample_id sometimes contains a 
    different type of analyte (e.g. whole genome amplification).  Instead, 
    extract the sample barcode from the filename"""
    sampleBarcode = manifestEntry["file"].split("/")[4]
    targetObj = analysis.find("TARGETS/TARGET")
    if not "refname" in targetObj.keys():
        targetObj.set("refname", sampleBarcode)
    if targetObj.get("refname") == "":
        targetObj.set("refname", sampleBarcode)


def runHasAnalysisObject(run, analysisObjDir):
    """Determine if the run refers to an analysis object, and if so, if
    that analysis object is present in the analysis directory"""
    hasObject = False
    identifiers = run.find("IDENTIFIERS")
    for thisId in identifiers:
        if re.search("^SRZ", thisId.text):
            # If there is an identifier that begins with an SRZ,
            # then that is an analysis object
            analysisAccession = thisId.text
            analysisPathname = analysisObjDir + "/" + analysisAccession + ".xml"
            if os.path.exists(analysisPathname):
                return(True)
    return(False)


def getOne( iterable ):
    """
    >>> getOne([])
    Traceback (most recent call last):
    ....
    ValueError: Argument has no elements
    >>> getOne([1])
    1
    >>> getOne([1,2])
    Traceback (most recent call last):
    ....
    ValueError: Argument has more than a single element
    """
    iterator = iter( iterable )
    try:
        result = next( iterator )
    except StopIteration:
        raise ValueError( 'Argument has no elements' )
    try:
        next( iterator )
    except StopIteration:
        return result
    else:
        raise ValueError( 'Argument has more than a single element' )


def getExperiment(runObj, experimentDir):
    """Retrieve the experiment object that corresponds to the indicated run"""
    experimentRef = getOne(runObj.iter("EXPERIMENT_REF"))
    experimentPath = experimentDir + "/" + experimentRef.get("accession") + ".xml"
    experiment = ET.parse(experimentPath).getroot()
    return(experiment)
    

def getSubObject(parentObject, tag):
    """Retrieve the indicated object from the parent.  Assumes that the
    tag has a single occurrence in the parent"""
    return(getOne(parentObject.iter(tag)))

def addSeqLabels(refAlign, inputDir):
    """Add the sequence label section to the reference align object"""
    seqLabels = ET.Element("SEQ_LABELS")
    assembly = refAlign.find("ASSEMBLY/STANDARD")
    refGenome = assembly.get("short_name")
    seqLabelsFile = "%s/BCCAGSC.%s.chromToAccession.txt" % (inputDir, 
                                                            refGenome)
    seqLabelData = csv.DictReader(open(seqLabelsFile), delimiter="\t")
    for row in seqLabelData:
        ET.SubElement(seqLabels, "SEQUENCE", 
                      attrib={"seq_label": row["seq_label"], 
                              "accession" : row["accession"]})
    refAlign.insert(2, seqLabels)
    


def createAnalysisObj(run, experimentObjDir, sampleBarcode, seqLabelsDataDir):
    """Create an analysis object from the indicated run object"""
    analysis = ET.Element("ANALYSIS", 
                          attrib={"center_name": run.get("center_name"),
                                  "alias": run.get("alias")})
    if "run_date" in run.keys():
        analysis.set("analysis_date", run.get("run_date"))
    title = ET.SubElement(analysis, "TITLE")
    title.text = "Reference alignment of " + run.get("alias")
    experiment = getExperiment(run, experimentObjDir)
    expStudyRef = getSubObject(experiment, "STUDY_REF")
    analysis.insert(1, copy.deepcopy(expStudyRef))
    description = ET.SubElement(analysis, "DESCRIPTION")
    description.text = "Automatically generated metadata object based on run accession %s" \
        % run.get("accession")
    analysisType = ET.SubElement(analysis, "ANALYSIS_TYPE")
    referenceAlignment = ET.SubElement(analysisType, "REFERENCE_ALIGNMENT")
    refAlignAssembly = ET.SubElement(referenceAlignment, "ASSEMBLY")
    ET.SubElement(refAlignAssembly, "STANDARD", 
                  attrib = { "short_name": "GRCh37-lite" } )
    runLabels = ET.SubElement(referenceAlignment, "RUN_LABELS")
    runSubObj = ET.SubElement(runLabels, "RUN", 
                              attrib={ "refcenter":"BCCAGSC",
                                       "refname": run.get("alias"),
                                       "accession": run.get("accession"),
                                       "read_group_label": run.get("alias") 
                                       } ) 
    addSeqLabels(referenceAlignment, seqLabelsDataDir)
    if run.find("PROCESSING") is not None:
        referenceAlignment.append(copy.deepcopy(getSubObject(run, 
                                                             "PROCESSING")))
        processing = getSubObject(referenceAlignment, "PROCESSING")
        directives = processing.find("DIRECTIVES")
        if directives is None:
            directives = ET.SubElement(processing,"DIRECTIVES")
        dUnaligned = ET.SubElement(directives, 
                                   "alignment_includes_unaligned_reads")
        dUnaligned.text = "true"
        dDuplicate = ET.SubElement(directives, 
                                   "alignment_marks_duplicate_reads")
        dDuplicate.text = "true"
        dFailed = ET.SubElement(directives, 
                                "alignment_includes_failed_reads")
        dFailed.text = "true"
    targets = ET.SubElement(analysis, "TARGETS")
    ET.SubElement(targets, "TARGET",
                  attrib={ "sra_object_type": "SAMPLE",
                           "refcenter": "NCI_TARGET",
                           "refname": sampleBarcode} )
    analysis.append(copy.deepcopy(getSubObject(run, "DATA_BLOCK")))
    return(analysis)
    

    


def createMissingAnalysisObjects(manifestByFilename, experimentObjDir, 
                                 runObjDir, analysisObjDir, seqLabelsDataDir):        
    """For each manifest entry, get the run object.  If the corresponding run
    XML is present in the indicated directory (indicating that it's a BCCAGSC
    submission), then check if the run references an analysis object.  If it
    does not, then create one."""
    for thisFile in manifestByFilename.keys():
        manifestData = manifestByFilename[thisFile]
        runAccession = manifestData["sra_accession"]
        runPathname = runObjDir + "/" + runAccession + ".xml"
        if os.path.exists(runPathname):
            run = ET.parse(runPathname).getroot()
            if not runHasAnalysisObject(run, analysisObjDir):
                analysisObj = createAnalysisObj(run,experimentObjDir, 
                                                manifestData["submitted_sample_id"],
                                                seqLabelsDataDir)
                fakeAnalysisAccession = "SRZ_" + run.get("accession")
                analysisPathname = analysisObjDir + "/" + fakeAnalysisAccession + ".xml"
                fp = open(analysisPathname, "w")
                fp.write(prettify(analysisObj))
                fp.close()


        

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputDir", type=str, 
                        help="Input directory for mal-packaged metadata")
    parser.add_argument('manifestFile', type=str,
                        help="Project manifest from dbGaP")
    parser.add_argument('seqLabelsDataDir', type=str,
                        help="Directory that contains the seq labels data")
    args = parser.parse_args()

    manifestByFilename = simplifyKeys(common.readDictOfDicts(args.manifestFile, 
                                                             delimiter=","))
    createMissingAnalysisObjects(manifestByFilename, 
                                 args.inputDir + "/experiment", 
                                 args.inputDir + "/run",
                                 args.inputDir + "/analysis",
                                 args.seqLabelsDataDir)
    for analysisFile in os.listdir(args.inputDir + "/analysis"):
        analysisPathname = args.inputDir + "/analysis/" + analysisFile
        print "working on", analysisFile
        analysisTree = ET.parse(analysisPathname)
        analysis = analysisTree.getroot()
        refAlign = analysis.find("ANALYSIS_TYPE/REFERENCE_ALIGNMENT")
        manifestThisFile = getManifestEntry(analysis, manifestByFilename)
        if manifestThisFile is not None:
            addTargetRefnameIfNeeded(analysis, manifestThisFile)
            run = getRunObject(analysis, manifestThisFile, args.inputDir)
            if run is  None:
                print "Run labels not found in manifest given run" 
            else:
                addRunLabelsIfNeeded(refAlign, run)
                print "Writing updated object to", analysisFile
        addSeqLabelsIfNeeded(refAlign, args.seqLabelsDataDir)
        analysisTree.write(analysisPathname)
        print "done writing"


if __name__ == '__main__':
    main()

    

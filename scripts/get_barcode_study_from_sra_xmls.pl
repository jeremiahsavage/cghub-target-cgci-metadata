#!/usr/bin/perl
#extract *first* barcode and library from SRA xml metadata
use strict;

my $dir = shift;
my $ids = shift;

my @ids;
open(IN,"<$ids");
while(my $line = <IN>)
{
	chomp($line);
	push(@ids,$line);
}
close(IN);

foreach my $srx (@ids)
{
	my @xmls = `ls $dir/$srx*.xml`;
	chomp(@xmls);
	my @barcodes;
	my @studies;
	my @tss_codes;
	foreach my $xml (@xmls)
	{
		my ($barcode,$study,$tss_code) = process_xml($xml);
		next if(length($barcode) == 0 && length($study) == 0 && length($tss_code)==0);
		push(@barcodes,$barcode) if(length($barcode) > 0);
		push(@studies,$study) if(length($study) > 0);
		push(@tss_codes,$tss_code) if($tss_code);
	}
	my $size = scalar @barcodes;
	my $barcodes = ($size == 0?"":($size > 1?join(":",@barcodes):$barcodes[0]));
	$size = scalar @studies;
	my $studies = ($size == 0?"":($size > 1?join(":",@studies):$studies[0]));
	$size = scalar @tss_codes;
	my $tss_codes = ($size == 0?"":($size > 1?join(":",@tss_codes):$tss_codes[0]));
	print "$srx\t$barcodes\t$studies\t$tss_codes\n";
}

sub process_xml
{
	my $xml = shift;
	my $barcode="";
	my $study="";
	my $tss_code="";
	open(IN,"<$xml");
	while(my $line = <IN>)
	{
		chomp($line);
		#if($line =~ /(TARGET-[0-9A-Za-z]+-[0-9A-Za-z]+-\w\w\w-\w\w\w)/i)
		#TARGET barcode and tss
		if(!$barcode && $line =~ /<((SAMPLE)|(EXTERNAL_ID))[^>]+"(TARGET-([0-9A-Za-z]{2})-[^"]+)"/)
		{	
			$barcode = $4;
			$tss_code = $5;
		}
		#CGCI barcode and tss
		elsif(!$barcode && $line =~ /<((SAMPLE)|(EXTERNAL_ID))[^>]+"(((HTMCP)|(BLGSP))-([0-9A-Za-z]{2})-[^"]+)"/)
		{
			$barcode = $4;
			$tss_code = $8;
		}
		if($line =~ /(phs\d+)/i)
		{	
			$study = $1;
		}
	}
	close(IN);
	return ($barcode,$study,$tss_code);
}

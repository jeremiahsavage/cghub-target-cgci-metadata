#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)


def aliasToAccession(objectDir):
    """For each metadata file in the indicated directory, map the object
    alias to its SRA accession.  Return a dictionary of these mappings"""
    mappings = dict()
    for metadataFile in os.listdir(objectDir):
        metadataXml = readXml(objectDir + "/" + metadataFile).getroot()
        alias = metadataXml.get("alias")
        assert(alias is not None)
        accession = metadataXml.get("accession")
        assert(accession is not None)
        assert(mappings.has_key(alias) is False)
        mappings[alias] = accession
    return(mappings)


def addAccessions(sourceObjDir, destObjDir, topAccessionMappings, 
                  refLabel, refAccessionMappings, warn=True):
    """For each object in the source dir, add accessions to the top-level
    from the top accession mappings.  If the ref label is not none and the
    refAccessionMappings is not none, use them to add accessions to the ref objects.
    Store results in the destination object dir.  If the source object is not in
    the mappings, generate a warning if warn is True."""
    for sourceFile in os.listdir(sourceObjDir):
        sourceTree = readXml(sourceDir + "/" + sourceFile)
        sourceXml = sourceTree.getroot()
        alias = sourceXml.get("alias")
        if topAccessionMappings.has_key(alias):
            sourceXml.set("accession", topAccessionMappings[alias])
        else:
            if warn:
                print "Warning: no accession found for alias %s, file %s" % (alias,
                                                                             sourceFile)
        if refLabel is not null:
             refXml = sourceXml.find(refLabel)
             assert(refXml is not None)
             refname = refXml.get("refname")
             if refAccessionMappings.has_key(refname):
                 sourceXml.set("accession", refAccessionMapping[refname])
             else:
                 if warn:
                     print "Warning: no accession found for refname %s, file %s" % 
                     (refname, sourceFile)
        sourceTree.write(destObjDir + "/" + sourceFile)
    

 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("sraObjects", type=str, 
                        help="""Directory containing the metadata objects from
                                SRA, with accessions.  This directory should
                                have three subdirectories: analysis, run,
                                and experiment.  Each of these subdirectories
                                should contain the SRA metadata objects of
                                the indicated type""")
    parser.add_argument("gscObjects", type=str, 
                        help="""Directory containing the metadata objects from
                                the center, lacking accessions.  These metadata
                                objects should be organized into the same three
                                subdirectories as the SRA objects.  Each 
                                object should have a refname""")
    parser.add_argument("revisedObjects", type=str,
                        help="""Directory containing the output metadata:
                                from the gsc per-object dir, but with the
                                accessions added back in""")
    args = parser.parse_args()
    #
    # Step 1: for each sra per-object subdir, build a dictionary that maps 
    # object alias to accession
    #
    analysisAliasToAccession = aliasToAccession(args.sraObjects + "\analysis")
    runAliasToAccession = aliasToAccession(args.sraObjects + "\run")
    experimentAliasToAccession = aliasToAccession(args.sraObjects + "\experiment")

    #
    # Step 2: make a set of output directories
    #
    if not os.path.exists(args.revisedObjects):
        os.mkdir(args.revisedObjects)
    os.mkdir(args.revisedObjects + "/analysis")
    os.mkdir(args.revisedObjects + "/run")
    os.mkdir(args.revisedObjects + "/experiemnt")

    #
    # Step 3: make new copies of the gsc objects, augmented with the SRA accessions
    # in the top-level and ref fields.
    #
    addAccessions(args.gscObjects + "/experiment", args.revisedObjects + "/experiment",
                  experimentAliasToAccession, None, None)
    addAccession(args.gscObjects + "/run", args.revisedObjects + "/run",
                 runAliasToAccession, "EXPERIMENT_REF", experimentAliasToAccession)
    addAccession(args.gscObjects + "/analysis", args.revisedObjects + "/analysis",
                 analysisAliasToAccession, "RUN_REF", runAliasToAccession, warn=False)
        
if __name__ == '__main__':
    main()

    

